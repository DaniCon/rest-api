Symfony MongoDB REST API
========================

## Installation
1. Build/run container with (with and without detached mode)
    ```bash
    $ docker-compose build
    $ docker-compose up -d
    ```

2. Update your system host file (add symfonymongodb.dev)
    ```bash
    $ sudo echo $(docker network inspect bridge | grep Gateway | grep -o -E '[0-9\.]+') "symfonymongodb.dev" >> /etc/hosts
    ```

3. Prepare the application.
    ```bash
    $ docker-compose exec php bash
    $ composer install
    $ chmod -R 777 var/cache var/logs var/sessions
    ```

## Usage

Run "docker-compose up -d"

1. Create a to do task:

    ```bash
    $ curl -X POST \
      http://symfonymongodb.dev/v1/tasks \
      -H 'cache-control: no-cache' \
      -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
      -H 'postman-token: a53aa981-435e-22d5-2897-2d964cbd4638' \
      -F 'title=See the game' \
      -F 'description=Chill out and have a drink to see the game!!' \
      -F duedate=2018/06/04 \
      -F completed=false
    ```
    
Response:

    ```
    {
        "id": "5b199f0e787eb81f82672344",
        "title": "See the game",
        "description": "Hang out with beer and pizza to see the game!!",
        "duedate": {
            "date": "2018-06-04 00:00:00.000000",
            "timezone_type": 3,
            "timezone": "America/Argentina/Buenos_Aires"
        },
        "completed": false,
        "created_at": {
            "date": "2018-06-07 18:09:34.000000",
            "timezone_type": 3,
            "timezone": "America/Argentina/Buenos_Aires"
        },
        "updated_at": null
    }
    ```

2. Retrieve the task:


    ```bash
    $ curl -X GET \
      http://symfonymongodb.dev/v1/tasks/5b199f0e787eb81f82672344 \
      -H 'cache-control: no-cache' \
      -H 'postman-token: d80b0fe8-e426-5784-b5d0-51b4ee55fb55'
    ```
    
3. Delete the task:


    ```bash
    $ curl -X DELETE \
      http://symfonymongodb.dev/v1/tasks/5b199f0e787eb81f82672344 \
      -H 'cache-control: no-cache' \
      -H 'postman-token: 5c63fa00-eff6-0a1d-61f0-410cb9e38b84'
    ```

4. Update the task:
    ```bash
    $ curl -X PUT \
      http://symfonymongodb.dev/v1/tasks/5b19a2ec787eb81f82672346 \
      -H 'cache-control: no-cache' \
      -H 'content-type: application/x-www-form-urlencoded' \
      -H 'postman-token: 03d82f93-6a64-c562-c9fb-f497f3ae3785' \
      -d 'title=Do%20the%20homework&description=Do%20the%20homework%20and%20study%20for%20the%20test!!&duedate=2018%2F06%2F20&completed=true'
    ```

5. Retrieve all tasks:


    ```bash
    $ curl -X GET \
      http://symfonymongodb.dev/v1/tasks \
      -H 'cache-control: no-cache' \
      -H 'postman-token: 855dce79-af63-e04e-968b-81f2cb4e87cc'
    ```