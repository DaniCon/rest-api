<?php

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\HasLifecycleCallbacks
 * @MongoDB\Document(repositoryClass="AppBundle\Repository\ProductRepository")
 */
class Task implements \JsonSerializable
{
    /**
     * @MongoDB\Id
     */
    protected $_id;
    
    /**
     * @MongoDB\Field(type="string")
     * @Assert\NotBlank(message="The field title cannot be blank")
     */
    protected $title;
    
    /**
     * @MongoDB\Field(type="string")
     */
    protected $description;
    
    /**
     * @MongoDB\Field(type="date")
     * @Assert\NotBlank(message="The field due_date cannot be blank")
     */
    protected $due_date;
    
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $completed;
    
    /**
     * @MongoDB\Field(type="date")
     */
    protected $created_at;
    
    /**
     * @MongoDB\Field(type="date")
     */
    protected $updated_at;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set dueDate
     *
     * @param date $dueDate
     * @return $this
     */
    public function setDueDate($dueDate)
    {
        $this->due_date = new \DateTime($dueDate);
        return $this;
    }

    /**
     * Get dueDate
     *
     * @return date $dueDate
     */
    public function getDueDate()
    {
        return $this->due_date;
    }

    /**
     * Set completed
     *
     * @param boolean $completed
     * @return $this
     */
    public function setCompleted($completed)
    {
        $this->completed = $completed;
        return $this;
    }

    /**
     * Get completed
     *
     * @return boolean $completed
     */
    public function getCompleted()
    {
        return $this->completed;
    }

    /**
     * Get createdAt
     *
     * @return timestamp $createdAt
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }
    
    public function setCreatedAt()
    {
        $this->created_at = new \DateTime();
        
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return timestamp $updatedAt
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }
    
    public function setUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }
    
    /**
     * @MongoDB\PrePersist
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
    }
    
    /**
     * @MongoDB\PreUpdate
     */
    public function preUpdated()
    {
        $this->updatedAt = new \DateTime();
    }
    
    public function jsonSerialize()
    {
        return array(
            'id' => $this->_id,
            'title' => $this->title,
            'description' => $this->description,
            'duedate' => $this->due_date,
            'completed' => $this->completed,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        );
    }

}
