<?php

namespace AppBundle\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;

/**
 * ProductRepository
 *
 * This class was generated by the Doctrine ODM. Add your own custom
 * repository methods below.
 */
class ProductRepository extends DocumentRepository
{
    public function getTasks()
    {
        return $this->createQueryBuilder()
//                ->field('title')->equals('titulo')
                ->limit(5)
                ->getQuery()
                ->execute();
    }
}
