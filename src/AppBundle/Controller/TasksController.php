<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Document\Task;

class TasksController extends FOSRestController
{
    /**
     * getTasksAction
     *
     * @return json
     */
    public function getTasksAction()
    {
        try {
            $cachedTasks = $this->get('memcache.default')->get('tasks');
            
            if (empty($cachedTasks)) {
                // Get the tasks from db
                $em = $this->get('doctrine_mongodb')->getManager();
                $tasks = $em->getRepository('AppBundle:Task')->findAll();
            
                if (!$tasks) {
                    return new JsonResponse('There is no tasks');
                }
                // Lets cache the response
                $this->get('memcache.default')->set('tasks', $tasks, 0, 7200);
                
                return new JsonResponse($tasks);
            }
            
            return new JsonResponse($cachedTasks);
            
        } catch (\Exception $e) {
            // If got error, lets show it
            return new JsonResponse('ERROR on retrieving taks: ' . $e->getMessage());
        }
    }
    
    /**
     * getTaskAction
     * @param int $id Task Id
     * 
     * @return json
     */
    public function getTaskAction($id)
    {
        try {
            // Get the task
            $em = $this->get('doctrine_mongodb')->getManager();
            $task = $em->getRepository('AppBundle:Task')->find($id);
            
            if (!$task) {
                return new JsonResponse('No task found with id '.$id);
            }
            return new JsonResponse($task);
        } catch (\Exception $e) {
            // If got error, lets show it
            return new JsonResponse('ERROR on retrieving taks: ' . $e->getMessage());
        }
    }
    
    /**
     * putTaskAction
     * @param int    $id      Task Id
     * @param object $request Request Object
     * 
     * @return json
     */
    public function putTaskAction($id, Request $request)
    {
        try {
            // Find the taks by id
            $em = $this->get('doctrine_mongodb')->getManager();
            $task = $em->getRepository('AppBundle:Task')->find($id);

            if (!$task) {
                // No task found
                return new JsonResponse('No task found with id '. $id);
            }
            
            // Get the request
            $title = $request->request->get('title');
            $description = $request->request->get('description');
            $dueDate = $request->request->get('duedate');
            $completed = $request->request->get('completed');
            
            // Set the task with the new data
            $task->setTitle($title);
            $task->setDescription($description);
            $task->setDueDate($dueDate);
            $task->setCompleted($completed);
            $task->setUpdatedAt();
            
            // Validate the inputs to be saved in the db
            $errorList = $this->get('validator')->validate($task);
            if (count($errorList) >= 1) {
                foreach ($errorList as $error) {
                    $data['errors'][] = $error->getMessage();
                }
                return new JsonResponse($data);
            }
            
            // Save task
            $em->flush();

            return new JsonResponse($task);
        } catch (Exception $e) {
            // If got error, lets show it
            return new JsonResponse('ERROR on updating taks: ' . $e->getMessage());
        }
        
    }
    
    /**
     * postTaskAction
     * @param object $request Request Object
     * 
     * @return json
     */
    public function postTaskAction(Request $request)
    {
        try {
            // Get the request
            $title = $request->request->get('title');
            $description = $request->request->get('description');
            $dueDate = $request->request->get('duedate');
            $completed = $request->request->get('completed');
            
            // Make the entity
            $task = new Task();
            $task->setTitle($title);
            $task->setDescription($description);
            $task->setDueDate($dueDate);
            $task->setCompleted($completed);
            $task->setCreatedAt();
            
            // Validate the inputs to be saved in the db
            $errorList = $this->get('validator')->validate($task);
            if (count($errorList) >= 1) {
                foreach ($errorList as $error) {
                    $data['errors'][] = $error->getMessage();
                }
                return new JsonResponse($data);
            }
            
            // Lets persist the object
            $em = $this->get('doctrine_mongodb')->getManager();
            $em->persist($task);
            $em->flush();
            
            return new JsonResponse($task);
        } catch (\Exception $e) {
            // If got error, lets show it
            return new JsonResponse('ERROR on creating taks: ' . $e->getMessage());
        }
    }
    
    /**
     * deleteTaskAction
     * @param int $id Task Id
     * 
     * @return json
     */
    public function deleteTaskAction($id)
    {
        try {
            $em = $this->get('doctrine_mongodb')->getManager();
            $task = $em->getRepository('AppBundle:Task')->find($id);

            if (!$task) {
                return new JsonResponse('No task found with id '.$id);
            }

            $em->remove($task);
            $em->flush();

            return new JsonResponse('Successfully Deleted task: ' . $id);
        } catch (\Exception $e) {
            return new JsonResponse('ERROR on deleting taks: ' . $e->getMessage());
        }
    }
}